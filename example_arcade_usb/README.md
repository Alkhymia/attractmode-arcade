Example complete config to copy to an arcade_usb. Runs everything fully from USB with the default ISO.

`install_to_hdd.sh` is a scripts that will copy over and edit the configs in this example so that everything runs from the HDD (useful only if you have already installed the Arcade Linux to HDD). You may need to edit this script to copy any extra additions you have.

Running the `install_to_hdd.sh` is as easy as:

1. `Ctrl+Alt+F2`
2. login with arcade:arcade
3. `cd /media/arcade_usb`
4. `./install_to_hdd.sh`
