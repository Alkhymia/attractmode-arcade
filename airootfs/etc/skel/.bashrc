export EDITOR=vim

bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

alias ls='ls --color=auto'
alias startx='startx -- -nocursor'
[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx
