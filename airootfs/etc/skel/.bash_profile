# System settings before starting X
. $HOME/.bashrc

PATH=$PATH:$HOME/bin

# set up alsa
/usr/bin/amixer sset Master Mono 50% unmute  &> /dev/null
/usr/bin/amixer sset Master 50% unmute  &> /dev/null
/usr/bin/amixer sset PCM 50% unmute &> /dev/null
